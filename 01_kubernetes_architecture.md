#  kubernetes/openshift 架构介绍
1. https://github.com/kubernetes/kubernetes/blob/release-1.1/docs/design/architecture.md   
2. https://github.com/kubernetes/kubernetes/blob/master/docs/user-guide/README.md  
3. https://docs.openshift.org/latest/architecture/index.html
2. https://blog.openshift.com/openshift-v3-deep-dive-docker-kubernetes/  

#  kubernetes/openshift关键组件  
1. https://github.com/kubernetes/kubernetes/blob/master/docs/design/architecture.md

#  kubernetes/openshift实验手册
1. https://github.com/openshift/origin/tree/master/examples
2. https://github.com/kubernetes/kubernetes/tree/master/examples

#  kubernetes/openshift培训手册
1. https://github.com/openshift/training
2. 

#  roadshow  
1. https://github.com/openshift/roadshow  

#  cloudfoundry servicebroker
1. https://github.com/asiainfoLDP/datafactory-servicebroker-example.git
